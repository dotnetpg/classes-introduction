﻿using System;

namespace ClassesIntroduction
{
    class Program
    {
        //Main program loop
        static void Main(string[] args)
        {
            //Create new object of class NewClass (and execute its constructor)
            NewClass obiekt = new NewClass();

            //Display values of PublicVariable and PrivateVariable claimed from function GetPrivateVariable()
            Console.WriteLine( obiekt.PublicVariable.ToString() );
            Console.WriteLine( obiekt.GetPrivateVariable().ToString() );

            //Display overriden ToString() method
            Console.WriteLine( obiekt.ToString() );

            //Create new object of class Server and define its _pages property via constructor
            Server server = new Server(new string[4] { "Page #1", "Page #2", "Page #3", "Page #4" });

            //Create new object of class Client and define its _server property via constructor
            Client client = new Client(server);

            //Execute Client's DisplayPage() method with page index 3
            client.DisplayPage(3);

            //Test some edgy cases:
            client.DisplayPage(-1);
            client.DisplayPage(123456);
            //
            client = new Client(null);
            client.DisplayPage(2);
            //
            server = new Server(null);
            client = new Client(server);
            client.DisplayPage(1);

            //ReadLine so the program won't end immediately
            Console.ReadLine();

            //Here will be executed the destructor of NewClass object
        }
    }
}
