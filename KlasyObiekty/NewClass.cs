﻿using System;

namespace ClassesIntroduction
{
    //NewClass class definition
    class NewClass
    {
        //Class properties
        public int PublicVariable;
        private int _privateVariable;

        //Public function returning _privateVariable
        public int GetPrivateVariable()
        {
            return _privateVariable;
        }

        //Public method setting the value of _privateVariable using parameter int value
        public void SetPrivateVariable(int value)
        {
            _privateVariable = value;
        }

        //Constructor with 0 parameters invoking the second constructor
        public NewClass() : this(12, 15)
        {
            //Display info to console
            Console.WriteLine("Created object with default values");
        }

        //Constructor with 2 parameters, setting the values of PublicVariable and _privateVariable
        public NewClass(int publicVariable, int privateVariable)
        {
            PublicVariable = publicVariable;
            _privateVariable = privateVariable;
        }

        //Destructor of the NewClass, invoked when the object is moved to Garbage Collector (freed from memory)
        ~NewClass()
        {
            Console.WriteLine("Destroyed object");
            Console.ReadLine();
        }

        //Overriden ToString() function with customized message to return
        public override string ToString()
        {
            string info = ("Public Variable: " + PublicVariable.ToString() +
                            ", Private Variable: " + _privateVariable.ToString());
            return info;
        }
    }
}
