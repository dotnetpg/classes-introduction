﻿using System;

namespace ClassesIntroduction
{
    //Client class definition
    class Client
    {
        //private Server class property (object reference)
        private Server _server;

        //Constructor with Server parameter to set the object reference
        public Client(Server server)
        {
            _server = server;
        }

        //Method displaying to console page from server of given index
        public void DisplayPage(int index)
        {
            //Debug and end the method if _server is null
            if (_server == null)
            {
                Console.WriteLine("Server object is NULL");
                return;
            }

            //Display the page previously checking if the page can be requested using CanRequestPage
            if (_server.CanRequestPage(index))
                Console.WriteLine(_server.RequestPage(index));
        }
    }
}
