﻿using System;

namespace ClassesIntroduction
{
    //Server class definition
    class Server
    {
        //Private _pages property - an array of strings
        private string[] _pages;

        //Constructor with an array of strings parameter to set the _pages
        public Server(string[] pages)
        {
            _pages = pages;
        }

        //Boolean function telling if the page of given index can be requested
        public bool CanRequestPage(int index)
        {
            if (IsPagesNull())
                return false;

            if (index > 0 && index < _pages.Length)
                return true;

            Console.WriteLine("Something went wrong! The index is out of range");
            return false;
        }

        //String function returning page of index, or empty string if page cannot be returned
        public string RequestPage(int index)
        {
            if (IsPagesNull())
                return "";
            return _pages[index];
        }

        //Boolean function displaying debug information and returning if the _pages is null
        private bool IsPagesNull()
        {
            if (_pages == null)
            {
                Console.WriteLine("Pages is NULL");
                return true;
            }
            return false;
        }
    }
}
